#include <petsc.h>

typedef struct{
    PetscInt        i; /* configuration Number */
    PetscInt        name; /* configuration Name */
    PetscScalar Raa; /* Self resistance of phase a */
    PetscScalar Rab; /* mutual resistance of phase a and phase b */
    PetscScalar Rac; /* mutual resistance of phase a and phase c */
    PetscScalar Rba; /* mutual resistance of phase b and phase a */
    PetscScalar Rbb; /* Self resistance of phase b */
    PetscScalar Rbc; /* mutual resistance of phase b and phase c */
    PetscScalar Rca; /* mutual resistance of phase c and phase a */
    PetscScalar Rcb; /*  mutual resistance of phase c and phase b */
    PetscScalar Rcc; /* Self resistance of phase c */
    PetscScalar Xaa; /* Self reactance of phase a */
    PetscScalar Xab; /* mutual reactance of phase a and phase b */
    PetscScalar Xac; /* mutual reactance of phase a and phase c */
    PetscScalar Xba; /* mutual reactance of phase b and phase a */
    PetscScalar Xbb; /* Self reactance of phase b */
    PetscScalar Xbc; /* mutual reactance of phase b and phase c */
    PetscScalar Xca; /* mutual reactance of phase c and phase a */
    PetscScalar Xcb; /*  mutual reactance of phase c and phase b */
    PetscScalar Xcc; /* Self reactance of phase c */
    PetscScalar Baa; /* Self shunt of phase a */
    PetscScalar Bab; /* mutual shunt of phase a and phase b */
    PetscScalar Bac; /* mutual shunt of phase a and phase c */
    PetscScalar Bba; /* mutual shunt of phase b and phase a */
    PetscScalar Bbb; /* Self shunt of phase b */
    PetscScalar Bbc; /* mutual shunt of phase b and phase c */
    PetscScalar Bca; /* mutual shunt of phase c and phase a */
    PetscScalar Bcb; /*  mutual shunt of phase c and phase b */
    PetscScalar Bcc; /* Self shunt of phase c */
}COND_CONFIG;

typedef struct{
    PetscInt from;
    PetscInt to;
    PetscInt    name; /* configuration Name */
    PetscScalar length;
    PetscInt fdpt; /* off diagonal position to*/
    PetscInt fdpf; /* off diagonal position from*/
}Dist_LINE;

typedef struct{
    PetscInt node_number;
    PetscInt id;
    PetscInt    nph; /* number of phase */
    
}NODES;

typedef struct{
    PetscInt id;
    PetscInt node;
    PetscInt nph;
    PetscScalar Pa;
    PetscScalar Qa;
    PetscScalar Pb;
    PetscScalar Qb;
    PetscScalar Pc;
    PetscScalar Qc;
}Dist_LOADS;

typedef struct{
    PetscInt node;
    PetscScalar Ca;
    PetscScalar Cb;
    PetscScalar Cc;
    
}CAP_BANK;


    

    






