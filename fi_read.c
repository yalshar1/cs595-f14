static char help[] = "Power flow for distribution system";


#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include "Dist_pf.h"
#include <petsc.h>
#include <petscmat.h>
#include <petscksp.h>
#include <petscvec.h>
#include <complex.h>
#include <petscsnes.h>


typedef struct {
    Mat Ybus;          /* test problem parameter */
} AppCtx;


extern PetscErrorCode FormJacobian1(SNES,Vec,Mat,Mat,void*);
extern PetscErrorCode FormFunction1(SNES,Vec,Vec,AppCtx*);


int main(int argc,char **args)
{

	char line[256];
    
    AppCtx         user;
    COND_CONFIG Cod[7];
    Dist_LINE dline[10];
    Dist_LOADS D_Load[11];
    CAP_BANK c_bank[2];
    NODES ND[11];
    

    
    PetscScalar ft2mil=0.000189393;
    
    int configi=0, linei=0, nodei=0, loadi=0, capi=0;
    
    
    
    int is_raw_conductor = 0; int is_raw_conductor_connection = 0; int is_raw_load = 0; int is_raw_cap = 0;
    int is_raw_node = 0;
    
    PetscInitialize(&argc,&args,(char*)0,help);
    PetscInt row[3];
    PetscInt col[3];
    
    PetscInt row1[6];
    PetscInt col1[6];
    
    PetscInt row2[46];
    PetscInt col2[46];
    
    PetscScalar values[9],values2[9],values3[9],values4[9] ;
    
    
    
    Mat m_Yn_TS;
    MatCreateSeqAIJ(PETSC_COMM_SELF, 52, 52, 52, NULL, &m_Yn_TS);
    MatSetOption(m_Yn_TS, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
    
    
    PetscScalar Zself_a_R, Zself_b_R, Zself_c_R, Zself_a_I, Zself_b_I, Zself_c_I;
    
    PetscScalar Zmut_ab_R, Zmut_ac_R,Zmut_ba_R, Zmut_bc_R,Zmut_ca_R, Zmut_cb_R;
    PetscScalar Zmut_ab_I, Zmut_ac_I,Zmut_ba_I, Zmut_bc_I,Zmut_ca_I, Zmut_cb_I;
    
    
    
    
    

	FILE *fp;
    
    // Beging of Reading the Raw Data File

	fp=fopen("prac1.raw", "r");
    
    
    /* Check for valid file */
    if (fp == NULL)  {
        printf("Can't open raw data file\n");
        exit(EXIT_FAILURE);
    }

	//fprintf(fp, line);
    
    while(fgets(line,212,fp) != NULL){

        if(strstr(line, "CONDUCTOR DATA") != NULL) {
            is_raw_conductor = 1;
            continue;
        }
        
        else if(strstr(line, "CONECTION") != NULL) {
            is_raw_conductor_connection = 1;
            continue;
        }
        else if(strstr(line, "NODES") != NULL) {
            is_raw_node = 1;
            continue;
        }
        
        else if(strstr(line, "LOADS") != NULL) {
            is_raw_load = 1;
            continue;
        }
        
        else if(strstr(line, "CAP_BANKS") != NULL) {
            is_raw_cap = 1;
            continue;
        }

        
        if (!is_raw_conductor){

            sscanf(line, "%d %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf ", &Cod[configi].i, &Cod[configi].name, &Cod[configi].Raa, &Cod[configi].Rab, &Cod[configi].Rac, &Cod[configi].Xaa, &Cod[configi].Xab, &Cod[configi].Xac, &Cod[configi].Baa, &Cod[configi].Bab, &Cod[configi].Bac, &Cod[configi].Rba, &Cod[configi].Rbb, &Cod[configi].Rbc, &Cod[configi].Xba, &Cod[configi].Xbb, &Cod[configi].Xbc, &Cod[configi].Bba, &Cod[configi].Bbb, &Cod[configi].Bbc, &Cod[configi].Rca, &Cod[configi].Rcb, &Cod[configi].Rcc, &Cod[configi].Xca, &Cod[configi].Xcb, &Cod[configi].Xcc, &Cod[configi].Bca, &Cod[configi].Bcb, &Cod[configi].Bcc);
            
            
            
            configi++;
        }
        else if (!is_raw_conductor_connection){
            sscanf(line,"%d %d %d %lf %d %d", &dline[linei].from, &dline[linei].to, &dline[linei].name, &dline[linei].length, &dline[linei].fdpt, &dline[linei].fdpf);
            
            
            linei++;
        }
        
        else if (!is_raw_node){
            sscanf(line,"%d %d %d", &ND[nodei].node_number, &ND[nodei].id, &ND[nodei].nph);
            

            nodei++;
        }

        
        else if (!is_raw_load){
            sscanf(line,"%d %d %d %lf %lf %lf %lf %lf %lf", &D_Load[loadi].id, &D_Load[loadi].node, &D_Load[loadi].nph, &D_Load[loadi].Pa, &D_Load[loadi].Qa, &D_Load[loadi].Pb, &D_Load[loadi].Qb,&D_Load[loadi].Pc, &D_Load[loadi].Qc);
            
            
            
            loadi++;

        }
        
        else if (!is_raw_cap){
            sscanf(line,"%d %lf %lf %lf", &c_bank[capi].node, &c_bank[capi].Ca, &c_bank[capi].Cb , &c_bank[capi].Cc );
            
            
            
            capi++;
            
        }


        
    }
    
    fclose(fp);
    
    // End of Reading the Raw Data File
    
    
    // Beging of the Detailed Modeling of Distribution System
    
    int count=0;
    for (int j=0; j < 12; j++){
        for (int i=0; i < 10; i++){
            if (ND[j].id == dline[i].from){
                if (dline[i].name == 601){
            
                    Zself_a_R=Cod[0].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[0].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[0].Rac/(dline[i].length*ft2mil);
                    Zmut_ba_R=Cod[0].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[0].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[0].Rbc/(dline[i].length*ft2mil);
                    Zmut_ca_R=Cod[0].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[0].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[0].Rcc/(dline[i].length*ft2mil);
            
           
            
                    
                    values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                    values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                    values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                    
                    for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
            
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    // of diagonal elements
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                   
                    // of diagonal elements
                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
                    
            
                
            
            
            
                    Zself_a_I=Cod[0].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[0].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[0].Xac/(dline[i].length*ft2mil);
                    Zmut_ba_I=Cod[0].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[0].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[0].Xbc/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[0].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[0].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[0].Xcc/(dline[i].length*ft2mil);
            
            

            
           
                    values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                    values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                    values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                    
                    for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}
                    
                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    

            
        }
                if (dline[i].name == 602){
        
                    Zself_a_R=Cod[1].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[1].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[1].Rac/(dline[i].length*ft2mil);
                    Zmut_ba_R=Cod[1].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[1].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[1].Rbc/(dline[i].length*ft2mil);
                    Zmut_ca_R=Cod[1].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[1].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[1].Rcc/(dline[i].length*ft2mil);
        
                    values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                    values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                    values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                    
                    for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                    
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                   
                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
        
        
        
        
                    Zself_a_I=Cod[1].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[1].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[1].Xac/(dline[i].length*ft2mil);
                    Zmut_ba_I=Cod[1].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[1].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[1].Xbc/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[1].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[1].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[1].Xcc/(dline[i].length*ft2mil);
        
        

                    values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                    values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                    values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                    for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}

                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
        
                   
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
        
        
        }
        
                if (dline[i].name == 603){
            
                    Zself_b_R=Cod[2].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[2].Rbc/(dline[i].length*ft2mil);
                    Zmut_cb_R=Cod[2].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[2].Rcc/(dline[i].length*ft2mil);
                    
                    
                    
                    values[0]=Zself_b_R; values[1]=Zmut_bc_R;
                    values[2]=Zmut_cb_R; values[3]=Zself_c_R;
                    
                    for(int p=0; p<4; p++){values3[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+1; col[1]=count*2+2;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col[0]=dline[i].fdpt*2+1; col[1]=dline[i].fdpt*2+2;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col[0]=count*2+4; col[1]=count*2+5;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=dline[i].fdpt*2+4; col[1]=dline[i].fdpt*2+5;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=dline[i].fdpt*2+2; col[1]=dline[i].fdpt*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    Zself_b_I=Cod[2].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[2].Xbc/(dline[i].length*ft2mil);
                    Zmut_cb_I=Cod[2].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[2].Xcc/(dline[i].length*ft2mil);
                    
                   
                    
                    values[0]=Zself_b_I; values[1]=Zmut_bc_I;
                    values[2]=Zmut_cb_I; values[3]=Zself_c_I;
                    
                    for(int p=0; p<4; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+4; col[1]=count*2+5;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+4; col[1]=dline[i].fdpt*2+5;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1; col[1]=count*2+2;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1; col[1]=dline[i].fdpt*2+2;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2; col[1]=dline[i].fdpt*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    

            
        }
        
                if (dline[i].name == 604){
            
                    Zself_a_R=Cod[3].Raa/(dline[i].length*ft2mil);  Zmut_ac_R=Cod[3].Rac/(dline[i].length*ft2mil);
                    
                    Zmut_ca_R=Cod[3].Rca/(dline[i].length*ft2mil);  Zself_c_R=Cod[3].Rcc/(dline[i].length*ft2mil);
                    

                    values[0]=Zself_a_R; values[1]=Zmut_ac_R;
                    values[2]=Zmut_ca_R; values[3]=Zself_c_R;
                    
                    for(int p=0; p<4; p++){values3[p]=values[p]*(-1);}
                    
                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2; col[1]=count*2+2;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+2;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=count*2+3; col[1]=count*2+5;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3; col[1]=dline[i].fdpt*2+5;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col[0]=dline[i].fdpt*2+2; col[1]=dline[i].fdpt*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    
                 
                    
                    
                    
                    
                    
                    Zself_a_I=Cod[3].Xaa/(dline[i].length*ft2mil);  Zmut_ac_I=Cod[3].Xac/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[3].Xca/(dline[i].length*ft2mil);  Zself_c_I=Cod[3].Xcc/(dline[i].length*ft2mil);
                    
                    
                   
                    values[0]=Zself_a_I; values[1]=Zmut_ac_I;
                    values[2]=Zmut_ca_I; values[3]=Zself_c_I;
                    
                    for(int p=0; p<4; p++){values4[p]=values[p]*(-1);}
                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+3; col[1]=count*2+5;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3; col[1]=dline[i].fdpt*2+5;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+2;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+2;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2; col[1]=dline[i].fdpt*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2; col[1]=dline[i].fdpt*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
            
        }

                if (dline[i].name == 605){
            
            
            
                    Zself_c_R=Cod[4].Rcc/(dline[i].length*ft2mil);
                    

                    
                    values[0]=Zself_c_R;
                    
                    for(int p=0; p<1; p++){values3[p]=values[p]*(-1);}
                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+5;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+5;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    

                    
                    
                    
                    
                    
                    Zself_c_I=Cod[4].Xcc/(dline[i].length*ft2mil);
                    
                    
                    
                    

                    values[0]=Zself_c_I;
                    
                    for(int p=0; p<1; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+5;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+5;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+2;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+3;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        col[0]=count*2+1;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        
                        col[0]=count*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                   
                    
            
            
        }

                if (dline[i].name == 606){
            
                    Zself_a_R=Cod[5].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[5].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[5].Rac/(dline[i].length*ft2mil);
                    Zmut_ba_R=Cod[5].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[5].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[5].Rbc/(dline[i].length*ft2mil);
                    Zmut_ca_R=Cod[5].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[5].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[5].Rcc/(dline[i].length*ft2mil);
                    

                    values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                    values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                    values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                    
                    for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                    
                    

                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    //dline[i].fdpt
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    

                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpt
                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                    
                    
                    Zself_a_I=Cod[5].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[5].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[5].Xac/(dline[i].length*ft2mil);
                    Zmut_ba_I=Cod[5].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[5].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[5].Xbc/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[5].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[5].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[5].Xcc/(dline[i].length*ft2mil);
                    
                    

                    values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                    values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                    values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                    
                    for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}
                    

                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    

                    
                    col[0]=dline[i].fdpt*2+3;col[1]=dline[i].fdpt*2+4; col[2]=dline[i].fdpt*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    

                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpt
                    
                    col[0]=dline[i].fdpt*2;col[1]=dline[i].fdpt*2+1; col[2]=dline[i].fdpt*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
            
        }

                if (dline[i].name == 607){
            
            
                    
                    Zself_a_R=Cod[6].Raa/(dline[i].length*ft2mil);
                    

                    
                    values[0]=Zself_a_R;
                    
                    for(int p=0; p<1; p++){values3[p]=values[p]*(-1);}

                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    Zself_a_I=Cod[6].Xaa/(dline[i].length*ft2mil);
                    
                    
                    
                    

                    values[0]=Zself_a_I;
                    
                    for(int p=0; p<1; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+3;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+3;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        
                        col[0]=dline[i].fdpt*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpt
                        col[0]=dline[i].fdpt*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    

                    
                    
            
        }

        
        
    }
            if (ND[j].id == dline[i].to){
                if (dline[i].name == 601){
                
                Zself_a_R=Cod[0].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[0].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[0].Rac/(dline[i].length*ft2mil);
                Zmut_ba_R=Cod[0].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[0].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[0].Rbc/(dline[i].length*ft2mil);
                Zmut_ca_R=Cod[0].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[0].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[0].Rcc/(dline[i].length*ft2mil);
                
                
                
               
                values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                
                for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                
                
                col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                
                
                // of diagonal elements
                
                col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                
                
                
                
                col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                
                // of diagonal elements
                
                col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                
                
                
                
                
                
                
                
                
                
                Zself_a_I=Cod[0].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[0].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[0].Xac/(dline[i].length*ft2mil);
                Zmut_ba_I=Cod[0].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[0].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[0].Xbc/(dline[i].length*ft2mil);
                Zmut_ca_I=Cod[0].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[0].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[0].Xcc/(dline[i].length*ft2mil);
                
                
                
                
 
                values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                
                for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}
                
                
                col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                
                
                MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                
                //dline[i].fdpf
                col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                
                
                col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                
                col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                
                MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                

                
                
                
            }
                if (dline[i].name == 602){
                    
                    Zself_a_R=Cod[1].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[1].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[1].Rac/(dline[i].length*ft2mil);
                    Zmut_ba_R=Cod[1].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[1].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[1].Rbc/(dline[i].length*ft2mil);
                    Zmut_ca_R=Cod[1].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[1].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[1].Rcc/(dline[i].length*ft2mil);
                    //MatCreateSeqAIJ(PETSC_COMM_SELF, nphase, nphase, 3, NULL, &m_Zblock_real);
                    values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                    values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                    values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                    
                    for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                    

                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    Zself_a_I=Cod[1].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[1].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[1].Xac/(dline[i].length*ft2mil);
                    Zmut_ba_I=Cod[1].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[1].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[1].Xbc/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[1].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[1].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[1].Xcc/(dline[i].length*ft2mil);
                    
                    
                    //MatCreateSeqAIJ(PETSC_COMM_SELF, nphase, nphase, 3, NULL, &m_Zblock_imag);
                    values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                    values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                    values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                    for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}

                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    

                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
                    
                    
                }
                
                if (dline[i].name == 603){
                    
                    Zself_b_R=Cod[2].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[2].Rbc/(dline[i].length*ft2mil);
                    Zmut_cb_R=Cod[2].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[2].Rcc/(dline[i].length*ft2mil);
                    
                    
                    values[0]=Zself_b_R; values[1]=Zmut_bc_R;
                    values[2]=Zmut_cb_R; values[3]=Zself_c_R;
                    
                    for(int p=0; p<4; p++){values3[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+1; col[1]=count*2+2;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1; col[1]=dline[i].fdpf*2+2;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+4; col[1]=count*2+5;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2+4; col[1]=dline[i].fdpf*2+5;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2+2; col[1]=dline[i].fdpf*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    
                    Zself_b_I=Cod[2].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[2].Xbc/(dline[i].length*ft2mil);
                    Zmut_cb_I=Cod[2].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[2].Xcc/(dline[i].length*ft2mil);
                    
                    

                    
                    values[0]=Zself_b_I; values[1]=Zmut_bc_I;
                    values[2]=Zmut_cb_I; values[3]=Zself_c_I;
                    
                    for(int p=0; p<4; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+4; col[1]=count*2+5;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+4; col[1]=dline[i].fdpf*2+5;
                        row[0]=count*2+1; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1; col[1]=count*2+2;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1; col[1]=dline[i].fdpf*2+2;
                        row[0]=count*2+4; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2; col[1]=dline[i].fdpf*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    

                    
                    
                    
                    
                }
                
                if (dline[i].name == 604){
                    
                    Zself_a_R=Cod[3].Raa/(dline[i].length*ft2mil);  Zmut_ac_R=Cod[3].Rac/(dline[i].length*ft2mil);
                    
                    Zmut_ca_R=Cod[3].Rca/(dline[i].length*ft2mil);  Zself_c_R=Cod[3].Rcc/(dline[i].length*ft2mil);
                    
                    //MatCreateSeqAIJ(PETSC_COMM_SELF, nphase, nphase, 2, NULL, &m_Zblock_real);
                    values[0]=Zself_a_R; values[1]=Zmut_ac_R;
                    values[2]=Zmut_ca_R; values[3]=Zself_c_R;
                    
                    for(int p=0; p<4; p++){values3[p]=values[p]*(-1);}

                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2; col[1]=count*2+2;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+2;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=count*2+3; col[1]=count*2+5;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3; col[1]=dline[i].fdpf*2+5;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+1;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2; col[1]=dline[i].fdpf*2+3;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    

                    
                    
                    
                    
                    
                    Zself_a_I=Cod[3].Xaa/(dline[i].length*ft2mil);  Zmut_ac_I=Cod[3].Xac/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[3].Xca/(dline[i].length*ft2mil);  Zself_c_I=Cod[3].Xcc/(dline[i].length*ft2mil);
                    
                    
                    //MatCreateSeqAIJ(PETSC_COMM_SELF, nphase, nphase, 2, NULL, &m_Zblock_imag);
                    values[0]=Zself_a_I; values[1]=Zmut_ac_I;
                    values[2]=Zmut_ca_I; values[3]=Zself_c_I;
                    
                    for(int p=0; p<4; p++){values4[p]=values[p]*(-1);}
                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+3; col[1]=count*2+5;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3; col[1]=dline[i].fdpf*2+5;
                        row[0]=count*2; row[1]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+2;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+2;
                        row[0]=count*2+3; row[1]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col[0]=count*2+2; col[1]=count*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2; col[1]=dline[i].fdpf*2+3;
                        row[0]=count*2; row[1]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2; col[1]=count*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2; col[1]=dline[i].fdpf*2+1;
                        row[0]=count*2+2; row[1]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row,2,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    

                    
                    
                    
                }
                
                if (dline[i].name == 605){
                    
                    
                    
                    Zself_c_R=Cod[4].Rcc/(dline[i].length*ft2mil);
                    

                    
                    values[0]=Zself_c_R;
                    
                    for(int p=0; p<1; p++){values3[p]=values[p]*(-1);}
                    
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+5;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+5;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    
                    Zself_c_I=Cod[4].Xcc/(dline[i].length*ft2mil);
                    
                    
                    
                    

                    values[0]=Zself_c_I;
                    
                    for(int p=0; p<1; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+5;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+5;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+2;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2;
                        row[0]=count*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+3;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        col[0]=count*2+1;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        
                        col[0]=count*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                }
                
                if (dline[i].name == 606){
                    
                    Zself_a_R=Cod[5].Raa/(dline[i].length*ft2mil); Zmut_ab_R=Cod[5].Rab/(dline[i].length*ft2mil); Zmut_ac_R=Cod[5].Rac/(dline[i].length*ft2mil);
                    Zmut_ba_R=Cod[5].Rba/(dline[i].length*ft2mil); Zself_b_R=Cod[5].Rbb/(dline[i].length*ft2mil); Zmut_bc_R=Cod[5].Rbc/(dline[i].length*ft2mil);
                    Zmut_ca_R=Cod[5].Rca/(dline[i].length*ft2mil); Zmut_cb_R=Cod[5].Rcb/(dline[i].length*ft2mil); Zself_c_R=Cod[5].Rcc/(dline[i].length*ft2mil);
                    

                    values[0]=Zself_a_R; values[1]=Zmut_ab_R; values[2]=Zmut_ac_R;
                    values[3]=Zmut_ba_R; values[4]=Zself_b_R; values[5]=Zmut_bc_R;
                    values[6]=Zmut_ca_R; values[7]=Zmut_cb_R; values[8]=Zself_c_R;
                    
                    for(int p=0; p<9; p++){values3[p]=values[p]*(-1);}
                    
                    
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    // MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                    

                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values3,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
 
                    
                    
                    
                    
                    
                    Zself_a_I=Cod[5].Xaa/(dline[i].length*ft2mil); Zmut_ab_I=Cod[5].Xab/(dline[i].length*ft2mil); Zmut_ac_I=Cod[5].Xac/(dline[i].length*ft2mil);
                    Zmut_ba_I=Cod[5].Xba/(dline[i].length*ft2mil); Zself_b_I=Cod[5].Xbb/(dline[i].length*ft2mil); Zmut_bc_I=Cod[5].Xbc/(dline[i].length*ft2mil);
                    Zmut_ca_I=Cod[5].Xca/(dline[i].length*ft2mil); Zmut_cb_I=Cod[5].Xcb/(dline[i].length*ft2mil); Zself_c_I=Cod[5].Xcc/(dline[i].length*ft2mil);
                    
                    values[0]=Zself_a_I; values[1]=Zmut_ab_I; values[2]=Zmut_ac_I;
                    values[3]=Zmut_ba_I; values[4]=Zself_b_I; values[5]=Zmut_bc_I;
                    values[6]=Zmut_ca_I; values[7]=Zmut_cb_I; values[8]=Zself_c_I;
                    
                    for(int p=0; p<9; p++){values4[p]=values[p]*(-1);}
                    
 
                    
                    col[0]=count*2+3;col[1]=count*2+4; col[2]=count*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2+3;col[1]=dline[i].fdpf*2+4; col[2]=dline[i].fdpf*2+5;
                    row[0]=count*2;row[1]=count*2+1; row[2]=count*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    col[0]=count*2;col[1]=count*2+1; col[2]=count*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //dline[i].fdpf
                    
                    col[0]=dline[i].fdpf*2;col[1]=dline[i].fdpf*2+1; col[2]=dline[i].fdpf*2+2;
                    row[0]=count*2+3;row[1]=count*2+4; row[2]=count*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row,3,col,values4,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    

                    
                    
                    
                }
                
                if (dline[i].name == 607){
                    
                    
                    
                    Zself_a_R=Cod[6].Raa/(dline[i].length*ft2mil);
                    

                    
                    values[0]=Zself_a_R;
                    
                    for(int p=0; p<1; p++){values3[p]=values[p]*(-1);}

                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values3,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    
                    

                    
                    
                    
                    
                    
                    Zself_a_I=Cod[6].Xaa/(dline[i].length*ft2mil);
                    
                    
                    
                    

                    values[0]=Zself_a_I;
                    
                    for(int p=0; p<1; p++){values4[p]=values[p]*(-1);}
                    
                    if (ND[j].nph > 2){
                        
                        col[0]=count*2+3;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+3;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col[0]=count*2+2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+2;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col[0]=count*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        
                        col[0]=dline[i].fdpf*2+1;
                        row[0]=count*2;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col[0]=count*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //dline[i].fdpf
                        col[0]=dline[i].fdpf*2;
                        row[0]=count*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row,1,col,values4,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
            }
        
        }
    
        count=count+ND[j].nph;
    
    }
    PetscInt counter=0;
    for (int j=0; j < 12; j++){
        for (int i=0; i < 10; i++){
            if (ND[j].id == dline[i].from){
                
                if (dline[i].name == 601){
                
                    
                    values2[0]=Cod[0].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[0].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[0].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[0].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[0].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[0].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[0].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[0].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[0].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    
                    

                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
 
                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                }
                
                
                
                if (dline[i].name == 602){
                    
                    
                    values2[0]=Cod[1].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[1].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[1].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[1].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[1].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[1].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[1].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[1].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[1].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                   
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
                }
                
                if (dline[i].name == 606){
                    
                    
                    values2[0]=Cod[5].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[5].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[5].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[5].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[5].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[5].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[5].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[5].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[5].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                   
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                }
                
                if (dline[i].name == 603){
                    
                    
                    
                    values2[0]=Cod[2].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[2].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[2]=Cod[2].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[3]=Cod[2].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    if (ND[j].nph > 2){
                        

                        
                        col1[0]=counter*2+4; col1[1]=counter*2+5;
                        row1[0]=counter*2+1; row1[1]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
 
                        
                        col1[0]=counter*2+1; col1[1]=counter*2+2;
                        row1[0]=counter*2+4; row1[1]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col1[0]=counter*2+2; col1[1]=counter*2+3;
                        row1[0]=counter*2; row1[1]=counter*2+1;
                        
 
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        

                        
                        col1[0]=counter*2; col1[1]=counter*2+1;
                        row1[0]=counter*2+2; row1[1]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                }
                
                
                if (dline[i].name == 604){
                    
                    
                    
                    values2[0]=Cod[3].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[3].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[2]=Cod[3].Bac*1e-6*((dline[i].length*ft2mil)/2); values2[3]=Cod[3].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+3; col1[1]=counter*2+5;
                        row1[0]=counter*2; row1[1]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2; col1[1]=counter*2+2;
                        row1[0]=counter*2+3; row1[1]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col1[0]=counter*2+2; col1[1]=counter*2+3;
                        row1[0]=counter*2; row1[1]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2; col1[1]=counter*2+1;
                        row1[0]=counter*2+2; row1[1]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                }
                
                if (dline[i].name == 605){
                    
                    
                values2[0]=Cod[4].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+5;
                        row1[0]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2+2;
                        row1[0]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col1[0]=counter*2+3;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }

                }
                
                if (dline[i].name == 607){
                    
                    
                    values2[0]=Cod[6].Baa*1e-6*((dline[i].length*ft2mil)/2);
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+3;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col1[0]=counter*2+2;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                    
                        col1[0]=counter*2;
                        row1[0]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                }




                
                //printf("from %d \n",dline[i].from);
                
                
                
            }
            
            if (ND[j].id == dline[i].to){
                
                if (dline[i].name == 601){
                    
                    
                    values2[0]=Cod[0].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[0].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[0].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[0].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[0].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[0].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[0].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[0].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[0].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    
           
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
 
                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                    
                    
                }
                
                
                
                if (dline[i].name == 602){
                    
                    
                    values2[0]=Cod[1].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[1].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[1].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[1].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[1].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[1].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[1].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[1].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[1].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
 
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                    
                    
 
                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                    
                    
                }
                
                if (dline[i].name == 606){
                    
                    
                    values2[0]=Cod[5].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[5].Bab*1e-6*((dline[i].length*ft2mil)/2); values2[2]=Cod[5].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[3]=Cod[5].Bba*1e-6*((dline[i].length*ft2mil)/2); values2[4]=Cod[5].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[5]=Cod[5].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[6]=Cod[5].Bca*1e-6*((dline[i].length*ft2mil)/2); values2[7]=Cod[5].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[8]=Cod[5].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
 
                    
                    col1[0]=counter*2+3;col1[1]=counter*2+4; col1[2]=counter*2+5;
                    row1[0]=counter*2;row1[1]=counter*2+1; row1[2]=counter*2+2;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

                    
                    col1[0]=counter*2;col1[1]=counter*2+1; col1[2]=counter*2+2;
                    row1[0]=counter*2+3;row1[1]=counter*2+4; row1[2]=counter*2+5;
                    
                    MatSetValues(m_Yn_TS,3,row1,3,col1,values2,ADD_VALUES);
                    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                    
                    
                    
                    
                }
                
                if (dline[i].name == 603){
                    
                    
                    
                    values2[0]=Cod[2].Bbb*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[2].Bbc*1e-6*((dline[i].length*ft2mil)/2);
                    values2[2]=Cod[2].Bcb*1e-6*((dline[i].length*ft2mil)/2); values2[3]=Cod[2].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    if (ND[j].nph > 2){
                        
 
                        
                        col1[0]=counter*2+4; col1[1]=counter*2+5;
                        row1[0]=counter*2+1; row1[1]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
 
                        
                        col1[0]=counter*2+1; col1[1]=counter*2+2;
                        row1[0]=counter*2+4; row1[1]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col1[0]=counter*2+2; col1[1]=counter*2+3;
                        row1[0]=counter*2; row1[1]=counter*2+1;
                        
                        
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col1[0]=counter*2; col1[1]=counter*2+1;
                        row1[0]=counter*2+2; row1[1]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                    }
                    
                }
                
                
                if (dline[i].name == 604){
                    
                    
                    
                    values2[0]=Cod[3].Baa*1e-6*((dline[i].length*ft2mil)/2); values2[1]=Cod[3].Bac*1e-6*((dline[i].length*ft2mil)/2);
                    values2[2]=Cod[3].Bac*1e-6*((dline[i].length*ft2mil)/2); values2[3]=Cod[3].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+3; col1[1]=counter*2+5;
                        row1[0]=counter*2; row1[1]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2; col1[1]=counter*2+2;
                        row1[0]=counter*2+3; row1[1]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    else if(ND[j].nph == 2){
                        
                        col1[0]=counter*2+2; col1[1]=counter*2+3;
                        row1[0]=counter*2; row1[1]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2; col1[1]=counter*2+1;
                        row1[0]=counter*2+2; row1[1]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,2,row1,2,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                }
                
                if (dline[i].name == 605){
                    
                    
                    values2[0]=Cod[4].Bcc*1e-6*((dline[i].length*ft2mil)/2);
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+5;
                        row1[0]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2+2;
                        row1[0]=counter*2+5;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col1[0]=counter*2+3;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                }
                
                if (dline[i].name == 607){
                    
                    
                    values2[0]=Cod[6].Baa*1e-6*((dline[i].length*ft2mil)/2);
                    
                    if (ND[j].nph > 2){
                        
                        col1[0]=counter*2+3;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+3;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    
                    if (ND[j].nph == 2){
                        
                        col1[0]=counter*2+2;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        //MatView(m_Yn_TS,PETSC_VIEWER_STDOUT_SELF);
                        
                    }
                    if (ND[j].nph == 1){
                        
                        col1[0]=counter*2+1;
                        row1[0]=counter*2;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                        
                        
                        col1[0]=counter*2;
                        row1[0]=counter*2+1;
                        
                        MatSetValues(m_Yn_TS,1,row1,1,col1,values2,ADD_VALUES);
                        MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
                        
                                               
                    }
                    
                }
                
                
                
                
                
                
                
                
                
            }
            
            
        }
        
        
        counter=counter+ND[j].nph;
       
    }
    

    
   // End of Detailed Modeling of Distribution System
    
   // Beging of Power Flow
    
    
    
    
    
    
    //load Modeling as constant impedence
    
    
     double complex z1, z2, z3;
    
    
    int count2=6;
    
    for (int i=0; i< 10; i++){
        
        
        
        if(D_Load[i].node== 632 || D_Load[i].node== 633 || D_Load[i].node== 675){
            
            //count2=count2+D_Load[i].nph*2;
            
            z1=(D_Load[i].Pa*1e3 - D_Load[i].Qa* I*1e3)/(2401.777*2401.777);
            z2=(D_Load[i].Pb*1e3 - D_Load[i].Qb* I*1e3)/(2401.777*2401.777);
            z3=(D_Load[i].Pc*1e3 - D_Load[i].Qc* I*1e3)/(2401.777*2401.777);
            
            values[0]= creal(z1); values[1]= 0; values[2]= 0;
            values[3]= 0; values[4]= creal(z2); values[5]= 0;
            values[6]= 0; values[7]= 0; values[8]= creal(z3);
            
            col[0]=count2; col[1]=count2+1; col[2]=count2+2;
            row[0]=count2; row[1]=count2+1; row[2]=count2+2;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+3; col[1]=count2+4; col[2]=count2+5;
            row[0]=count2+3; row[1]=count2+4; row[2]=count2+5;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            values[0]= cimag(z1); values[1]= 0; values[2]= 0;
            values[3]= 0; values[4]= cimag(z2); values[5]= 0;
            values[6]= 0; values[7]= 0; values[8]= cimag(z3);
            
            for(int k=0; k < 9; k++){values2[k]=values[k]*(-1);}
            
            col[0]=count2+3; col[1]=count2+4; col[2]=count2+5;
            row[0]=count2; row[1]=count2+1; row[2]=count2+2;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2; col[1]=count2+1; col[2]=count2+2;
            row[0]=count2+3; row[1]=count2+4; row[2]=count2+5;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            

            
            count2=count2+D_Load[i].nph*2;
            
        }
        
        else if(D_Load[i].node== 671){
            
            //count2=count2+D_Load[i].nph*2;
            
            z1=(D_Load[i].Pa*1e3 - D_Load[i].Qa* I*1e3)/(4160*4160);
            z2=(D_Load[i].Pb*1e3 - D_Load[i].Qb* I*1e3)/(4160*4160);
            z3=(D_Load[i].Pc*1e3 - D_Load[i].Qc* I*1e3)/(4160*4160);
            
            values[0]= creal(z1); values[1]= 0; values[2]= 0;
            values[3]= 0; values[4]= creal(z2); values[5]= 0;
            values[6]= 0; values[7]= 0; values[8]= creal(z3);
            
            
            col[0]=count2; col[1]=count2+1; col[2]=count2+2;
            row[0]=count2; row[1]=count2+1; row[2]=count2+2;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+3; col[1]=count2+4; col[2]=count2+5;
            row[0]=count2+3; row[1]=count2+4; row[2]=count2+5;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            values[0]= cimag(z1); values[1]= 0; values[2]= 0;
            values[3]= 0; values[4]= cimag(z2); values[5]= 0;
            values[6]= 0; values[7]= 0; values[8]= cimag(z3);
            
            for(int k=0; k < 9; k++){values2[k]=values[k]*(-1);}
            
            col[0]=count2+3; col[1]=count2+4; col[2]=count2+5;
            row[0]=count2; row[1]=count2+1; row[2]=count2+2;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2; col[1]=count2+1; col[2]=count2+2;
            row[0]=count2+3; row[1]=count2+4; row[2]=count2+5;
            
            MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);

            
            count2=count2+D_Load[i].nph*2;
            
        }
        
        else if(D_Load[i].node== 652){
            
            //count2=count2+D_Load[i].nph*2;
            
            z1=(D_Load[i].Pa*1e3 - D_Load[i].Qa* I*1e3)/(4160*4160);
            
            
            values[0]= creal(z1);
            
            col[0]=count2;
            row[0]=count2;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+1;
            row[0]=count2+1;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            values[0]= cimag(z1);
            
            values2[0]=values[0]*(-1);
            
            col[0]=count2+1;
            row[0]=count2;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2;
            row[0]=count2+1;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            

            
            count2=count2+D_Load[i].nph*2;
            
        }
        
        else if(D_Load[i].node== 611){
            

            
            z1=(D_Load[i].Pc*1e3 - D_Load[i].Qc* I*1e3)/(2401.777*2401.777);
            
            
            values[0]= creal(z1);
            
            col[0]=count2;
            row[0]=count2;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+1;
            row[0]=count2+1;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            values[0]= cimag(z1);
            
            values2[0]=values[0]*(-1);
            
            col[0]=count2+1;
            row[0]=count2;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2;
            row[0]=count2+1;
            
            MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            

            
            count2=count2+D_Load[i].nph*2;
            
        }
        
        else if(D_Load[i].node== 645){
            

            
            z1=(D_Load[i].Pb*1e3 - D_Load[i].Qb* I*1e3)/(2401.777*2401.777);
            
            
            values[0]= 0; values[1]= 0;
            values[2]= 0; values[3]= creal(z1);
            
            col[0]=count2; col[1]=count2+1;
            row[0]=count2; row[1]=count2+1;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+2; col[1]=count2+3;
            row[0]=count2+2; row[1]=count2+3;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            values[0]= 0; values[1]= 0;
            values[2]= 0; values[3]= cimag(z1);
            
            
            for(int k=0; k < 4; k++){values2[k]=values[k]*(-1);}
            
            col[0]=count2+2; col[1]=count2+3;
            row[0]=count2; row[1]=count2+1;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2; col[1]=count2+1;
            row[0]=count2+2; row[1]=count2+3;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            //printf("v 645 %d \n",count2);
            

            
            count2=count2+D_Load[i].nph*2;
            
        }
        
        else if(D_Load[i].node== 646){
            
            //count2=count2+D_Load[i].nph*2;
            
            z1=(D_Load[i].Pb*1e3 - D_Load[i].Qb* I*1e3)/(4160*4160);
            
            values[0]= 0; values[1]= 0;
            values[2]= 0; values[3]= creal(z1);
            
            col[0]=count2; col[1]=count2+1;
            row[0]=count2; row[1]=count2+1;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2+2; col[1]=count2+3;
            row[0]=count2+2; row[1]=count2+3;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            values[0]= 0; values[1]= 0;
            values[2]= 0; values[3]= cimag(z1);
            
            
            for(int k=0; k < 4; k++){values2[k]=values[k]*(-1);}
            
            col[0]=count2+2; col[1]=count2+3;
            row[0]=count2; row[1]=count2+1;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values2,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            col[0]=count2; col[1]=count2+1;
            row[0]=count2+2; row[1]=count2+3;
            
            MatSetValues(m_Yn_TS,2,row,2,col,values,ADD_VALUES);
            MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
            MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
            
            
            
            count2=count2+D_Load[i].nph*2;
            
        }

        else{
            
            count2=count2+D_Load[i].nph*2;
            
        }


        
    }
    
    // Capacitance Bank Inclusion in the Bus Admitance Matrix
    // Since we only have two Capicatnce Banks, I am adding them without manually.
    
    values[0]=(c_bank[0].Ca*1e3)/(2401.777*2401.777); values[1]=0; values[2]=0;
    values[3]=0; values[4]=(c_bank[0].Cb*1e3)/(2401.777*2401.777); values[5]=0;
    values[6]=0; values[7]=0; values[8]=(c_bank[0].Cc*1e3)/(2401.777*2401.777);
    
    for(int k=0; k < 9; k++){values2[k]=values[k]*(-1);}
    
    col[0]=32+3; col[1]=32+4; col[2]=32+5;
    row[0]=32; row[1]=32+1; row[2]=32+2;
    
    MatSetValues(m_Yn_TS,3,row,3,col,values2,ADD_VALUES);
    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
    
    col[0]=32; col[1]=32+1; col[2]=32+2;
    row[0]=32+3; row[1]=32+4; row[2]=32+5;
    
    MatSetValues(m_Yn_TS,3,row,3,col,values,ADD_VALUES);
    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
    
    //
    values[0]=(c_bank[1].Cc*1e3)/(2401.777*2401.777);
    
    values2[0]=values[0]*(-1);
    
    col[0]=44+1;
    row[0]=44;
    
    MatSetValues(m_Yn_TS,1,row,1,col,values2,ADD_VALUES);
    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
    
    col[0]=44;
    row[0]=44+1;
    
    MatSetValues(m_Yn_TS,1,row,1,col,values,ADD_VALUES);
    MatAssemblyBegin(m_Yn_TS,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (m_Yn_TS,MAT_FINAL_ASSEMBLY);
    
    //Mat *ybus[52];
    
    
    for (int i=0; i<46; i++){
        
        row2[i]=i+6;
        col2[i]=i+6;
        
    }
    
    // Eliminate the rows and columns associated with the refrence bus
    
    PetscScalar v[2116];
    //PetscScalar *v;
    
    
    
    MatGetValues(m_Yn_TS,46,row2,46,col2,v);
    
    Mat Ybus;
    MatCreateSeqAIJ(PETSC_COMM_SELF, 46, 46, 46, NULL, &Ybus);
    MatSetOption(Ybus, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE);
    
    for (int i=0; i<46; i++){
        
        row2[i]=i;
        col2[i]=i;
        
    }

    
    MatSetValues(Ybus,46,row2,46,col2,v,ADD_VALUES);
    MatAssemblyBegin(Ybus,MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd  (Ybus,MAT_FINAL_ASSEMBLY);
    user.Ybus=Ybus;
    
   
    
    // using the nonlinear solver to evaluate the system
    
    SNES           snes;
    KSP            ksp;
    PC             pc;
    Vec            x,r;
    Mat            J;
    PetscInt its;
    PetscScalar *xx;
    
    
    SNESCreate(PETSC_COMM_WORLD,&snes);
    
    /*
     Create vectors for solution and nonlinear function
     */
    
    VecCreate(PETSC_COMM_WORLD,&x);
    VecSetSizes(x,PETSC_DECIDE,46);
    VecSetFromOptions(x);
    VecDuplicate(x,&r);
    
    /*
     Create Jacobian matrix data structure
     */
    /*MatCreate(PETSC_COMM_WORLD,&J);
    MatSetSizes(J,PETSC_DECIDE,PETSC_DECIDE,46,46);
    MatSetFromOptions(J);
    MatSetUp(J);*/
    
    /*
     Set function evaluation routine and vector.
     */
    SNESSetFunction(snes,r,FormFunction1,&user);
    /*
     Set Jacobian matrix data structure and Jacobian evaluation routine
     */
    //SNESSetJacobian(snes,J,J,FormJacobian1,NULL);
    
    /*
     Set linear solver defaults for this problem. By extracting the
     KSP and PC contexts from the SNES context, we can then
     directly call any KSP and PC routines to set various options.
     */
    SNESGetKSP(snes,&ksp);
    KSPGetPC(ksp,&pc);
    PCSetType(pc,PCNONE);
    KSPSetTolerances(ksp,1.e-4,PETSC_DEFAULT,PETSC_DEFAULT,50);
    
    /*
     Set SNES/KSP/KSP/PC runtime options, e.g.,
     -snes_view -snes_monitor -ksp_type <ksp> -pc_type <pc>
     These options will override those specified above as long as
     SNESSetFromOptions() is called _after_ any other customization
     routines.
     */
    SNESSetFromOptions(snes);
    
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Evaluate initial guess; then solve nonlinear system
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        VecGetArray(x,&xx);
        xx[0] = 1; xx[1] = -0.5; xx[2] = -0.5; xx[3] = 0; xx[4] = -0.86602; xx[5] = 0.86602;
        xx[6] = 1; xx[7] = -0.5; xx[8] = -0.5; xx[9] = 0; xx[10] = -0.86602; xx[11] = 0.86602;
        xx[12] = -0.5; xx[13] = -0.5; xx[14] = -0.86602; xx[15] = 0.86602;
        xx[16] = -0.5; xx[17] = -0.5; xx[18] = -0.86602; xx[19] = 0.86602;
        xx[20] = 1; xx[21] = -0.5; xx[22] = -0.5; xx[23] = 0; xx[24] = -0.86602; xx[25] = 0.86602;
        xx[26] = 1; xx[27] = -0.5; xx[28] = -0.5; xx[29] = 0; xx[30] = -0.86602; xx[31] = 0.86602;
        xx[32] = 1; xx[33] = -0.5; xx[34] = 0; xx[35] = 0.86602;
        xx[36] = 1; xx[37] = 0;
        xx[38] = -0.5; xx[39] = 0.86602;
        xx[40] = 1; xx[41] = -0.5; xx[42] = -0.5; xx[43] = 0; xx[44] = -0.86602; xx[45] = 0.86602;
    
    
    
    
        VecRestoreArray(x,&xx);
    
    
    
    
        SNESSolve(snes,NULL,x);
        SNESGetIterationNumber(snes,&its);
    
        Vec f;
    
    
        VecView(x,PETSC_VIEWER_STDOUT_WORLD);
        SNESGetFunction(snes,&f,0,0);

    
        PetscPrintf(PETSC_COMM_WORLD,"Number of SNES iterations = %D\n",its);
        VecDestroy(&x); VecDestroy(&r);
        MatDestroy(&J); SNESDestroy(&snes);
        MatDestroy(&m_Yn_TS); MatDestroy(&Ybus);
    

    
    

	return 0;
}




#undef __FUNCT__
#define __FUNCT__ "FormFunction1"
/*
 FormFunction1 - Evaluates nonlinear function, F(x).
 Input Parameters:
 .  snes - the SNES context
 .  x    - input vector
 .  ctx  - optional user-defined context
 Output Parameter:
 .  f - function vector
 */
PetscErrorCode FormFunction1(SNES snes,Vec x,Vec f, AppCtx *user)
{
    PetscErrorCode    ierr;
    const PetscScalar *xx;
    PetscScalar       *ff;
    //PetscScalar       *vv;
    Mat Ybus;
    Ybus=user->Ybus;
    
    ierr = VecGetArrayRead(x,&xx);CHKERRQ(ierr);
    //ierr = VecGetArrayRead(v,&vv);CHKERRQ(ierr);
    ierr = VecGetArray(f,&ff);CHKERRQ(ierr);
    MatMult(Ybus,x,f);
    
    /* Restore vectors */
    ierr = VecRestoreArrayRead(x,&xx);CHKERRQ(ierr);
    ierr = VecRestoreArray(f,&ff);CHKERRQ(ierr);
    return 0;
}













